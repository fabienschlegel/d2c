import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faTwitter,
  faLinkedin,
  faGitlab,
} from "@fortawesome/free-brands-svg-icons";

import React from "react";
import PropTypes from "prop-types";

const Footer = ({ copyrights }) => (
  <footer>
    {copyrights ? (
      <div
        dangerouslySetInnerHTML={{
          __html: copyrights,
        }}
      />
    ) : (
      <>
        <span className="footerCopyrights">© 2021 Fabien Schlegel</span>
        <span className="footerCopyrights">
          <a
            className="footer-icon"
            title="My Twitter account"
            href="https://twitter.com/fabienschlegel"
          >
            <FontAwesomeIcon icon={faTwitter} size="2x" alt="Twitter" />
          </a>
          <a
            className="footer-icon"
            title="My LinkedIn profile"
            href="https://www.linkedin.com/in/fabien-schlegel"
          >
            <FontAwesomeIcon icon={faLinkedin} size="2x" alt="LinkedIn" />
          </a>
          <a
            className="footer-icon"
            title="My Gitlab account"
            href="https://gitlab.com/Humch"
          >
            <FontAwesomeIcon icon={faGitlab} size="2x" alt="Gitlab" />
          </a>
        </span>
      </>
    )}
  </footer>
);

Footer.propTypes = {
  copyrights: PropTypes.string,
};

export default Footer;
